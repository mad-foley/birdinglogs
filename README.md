## A Birder app designed for my SWE portfolio.

App: Designed for casual birders to look at, log and learn a little bit about the birds they see. Basic functionality is to search birds, add comments/sightings and adding birds to a wishlist.

Tools: FastAPI, PostgreSQL and React.js

Over 900 North American bird species retrieved from publically available wikipediaapi and wikimedia restful api. Birds have been stored into a postgresql database and allows for users to add missing birds. Data file is currently hidden.


## Currently Working on:
Basic front-end functionality. Backend and authentication done.


Disclaimer:
This project is not for profit/scientific accuracy and is only to practice coding. All information and pictures come from each bird's wikipedia. Copyrighted images have been manually removed.
